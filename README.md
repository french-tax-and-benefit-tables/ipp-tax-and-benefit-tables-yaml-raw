# ipp-tax-and-benefit-tables-yaml-raw

Raw YAML files generated from IPP's XLS-formatted tax and benefit tables.

IPP = [Institut des politiques publiques](http://www.ipp.eu/en/)

**Note:** Generally, you don't want to use these raw YAML files directly. Use
**[clean YAML files](https://git.framasoft.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-yaml-clean)**
instead.

Original tax and benefit tables:
- English: http://www.ipp.eu/en/tools/ipp-tax-and-benefit-tables/
- French: http://www.ipp.eu/fr/outils/baremes-ipp/

These YAML files have been automatically generated by script `ipp_tax_and_benefit_tables_xls_to_yaml.py` from package
[ipp-tax-and-benefit-tables-converters](https://git.framasoft.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-converters)
developped by [Etalab](http://www.etalab.gouv.fr) for [OpenFisca](http://www.openfisca.fr), a versatile micro-simulation
software.

## Data License

Licence ouverte / Open Licence <http://www.etalab.gouv.fr/licence-ouverte-open-licence>
